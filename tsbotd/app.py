import time

from fastapi import FastAPI, Request
from prometheus_client import (
    CONTENT_TYPE_LATEST,
    Counter,
    Histogram,
    generate_latest,
)
from starlette.responses import Response
import mlflow
from dotenv import load_dotenv

load_dotenv()
app = FastAPI()

REQUEST_COUNT = Counter(
    "request_count", "Total number of requests", ["method", "endpoint"]
)
REQUEST_LATENCY = Histogram(
    "request_latency_seconds", "Request latency", ["method", "endpoint"]
)


model_uri = "models:/ETNA-CatBoost/latest"
loaded_model = mlflow.pyfunc.load_model(model_uri)


@app.middleware("http")
async def add_prometheus_metrics(request: Request, call_next):
    method = request.method
    endpoint = request.url.path
    REQUEST_COUNT.labels(method=method, endpoint=endpoint).inc()
    start_time = time.time()
    response = await call_next(request)
    REQUEST_LATENCY.labels(method=method, endpoint=endpoint).observe(
        time.time() - start_time
    )
    return response


@app.get("/metrics")
async def metrics():
    return Response(generate_latest(), media_type=CONTENT_TYPE_LATEST)


@app.post("/predict")
async def predict(data: dict):
    preds = loaded_model.predict(None)
    return {"predictions": preds.df["mean_price"]["target"].values.tolist()}
